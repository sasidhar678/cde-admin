var app = angular.module('cde', []);

app.controller('loginController', function ($scope, $http) {

    $scope.email = '';
    $scope.password = '';
    $scope.error = '';

    $scope.doLogin = function () {
        $scope.error = '';

        $http.post('/auth/login', {
            email: $scope.email,
            password: $scope.password
        }).then(function (response) {
            //    200
            console.log('Success');

        }, function (response) {
            //    other 200
            console.log('Error');
            $scope.error = response.data.error;
        });
    }

});

app.controller('usersController', function ($scope, $http) {

    $scope.users = [];
    $scope.activeUser = null;

    $http.get('/users').then(function (successResponse) {
        var data = successResponse.data;
        $scope.users = data;
        console.log('Success');
    }, function (errorResponse) {
        console.log('Errors');
    });


    $scope.editUser = function (user) {
        $scope.activeUser = user;
        console.log('edit', user.email);
    };

    $scope.deleteUser = function (user) {
        $http.delete('/users/' + user._id).then(function (successResponse) {

            _.remove($scope.users, function (item) {
                return item._id === user._id;
            });

        }, function (errorResponse) {
            console.log('Error');
        })
    };

    $scope.saveUser = function () {

        $http.put('/users', $scope.activeUser).then(function (successResponse) {
            console.log('Success');
        }, function (errorResponse) {
            console.log('Error');
        });

    }

});