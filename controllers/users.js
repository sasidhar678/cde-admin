const mongoose = require('mongoose');
const UserModel = require('../mongo-models/users');
mongoose.Promise = global.Promise;
const jwt = require('jsonwebtoken');

const authController = function (req, res) {

    const data = req.body;

    const db = mongoose.connect('mongodb://localhost/cde', {useMongoClient: true});

    UserModel.findOne({email: data.email, password: data.password}, (err, doc) => {

        if (err) {
            res.status(500).json({error: 'Internal Server error'});
        } else if (!doc) {
            res.status(401).json({error: 'User does not exists'});
        } else {
            jwt.sign({email: doc.email, id: doc._id}, '1511926640233', function (err, token) {
                console.log(token);
                res.status(200).json({auth_toke: token, user: doc});
            });
        }
        db.close();
    });

};

const updateProfile = function (req, res) {

};

const getUsersList = function (req, res) {

    const db = mongoose.connect('mongodb://localhost/cde', {useMongoClient: true});

    UserModel.find()
        .select('email name mobile')
        .exec((err, docs) => {
            if (err) {
                res.status(500).json({error: 'Internal Server error'});
            } else {
                res.status(200).json(docs);
            }
            db.close();
        });
};

const updateUser = function (req, res) {

    const data = req.body;

    const db = mongoose.connect('mongodb://localhost/cde', {useMongoClient: true});

    UserModel.findByIdAndUpdate(data._id, {
        email: data.email,
        name: data.name,
        mobile: data.mobile
    }, function (err, doc) {
        if (err) {
            res.status(500).json({error: 'Internal Server error'});
        } else {
            res.status(200).json(doc);
        }
        db.close();
    });

};

const deleteUser = function (req, res) {

    const id = req.params.id;

    const db = mongoose.connect('mongodb://localhost/cde', {useMongoClient: true});

    UserModel.findByIdAndRemove(id, function (err) {
        if (err) {
            res.status(500).json({error: 'Internal Server error'});
        } else {
            res.status(200).json({message: 'deleted'});
        }
        db.close();
    });

};

module.exports = {
    authController: authController,
    updateProfile: updateProfile,
    getUsers: getUsersList,
    updateUser: updateUser,
    deleteUser: deleteUser
};