# CDE Team Events Admin panel #

1. Clone project
```
git clone https://sasidhar678@bitbucket.org/sasidhar678/cde-admin.git
```
2. Navigate to root directory
3. Install node dependencies
```
npm install
```
4. Run Mongo DB server
5. Start Node server
```
npm start
```
