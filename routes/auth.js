var express = require('express');
var router = express.Router();
var middleware = require('../middlewares/router-middlewares');
var userController = require('../controllers/users');

/* GET home page. */
router.post('/', middleware.authMiddleware, userController.authController);

module.exports = router;
