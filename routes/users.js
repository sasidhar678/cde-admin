var express = require('express');
var router = express.Router();
var usersController = require('../controllers/users');

router.route('/')
    .get(usersController.getUsers)
    .put(usersController.updateUser);

router.route('/:id')
    .delete(usersController.deleteUser);

module.exports = router;
