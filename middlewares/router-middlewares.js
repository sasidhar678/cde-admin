const authMiddleware = function (req, res, next) {
    const data = req.body;
    if (data.email && data.password) {
        next();
    } else {
        res.status(400).json({error: 'Missing required fields'});
    }
};

module.exports = {
    authMiddleware: authMiddleware
};